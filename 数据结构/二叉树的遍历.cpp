/**
 * @file BinaryTree.cpp
 * @author BadFatCat0919 (543015378@qq.com)
 * @brief 二叉树的创建与遍历
 * @date 2021-10-06
 */

/* 头文件 */
#include <stdio.h>
#include <stdlib.h> // malloc()函数在这个头文件里

/* 数据类型定义 */
typedef int ElemType;

/* 常量定义 */
const int MAX = 100;

/**
 * @brief 二叉树数据结构
 */
typedef struct BiTNode{
	ElemType data;
	struct BiTNode *l, *r;
}BiTNode, *BiTree;

/**
 * @brief 根据中序和前序遍历序列递归创建二叉树
 * @param LNR 存放中序遍历序列的数组
 * @param LNR_start 序列在数组中的起始位置
 * @param LNR_end 序列在数组中的结束位置
 * @param NLR 存放前序遍历序列的数组
 * @param NLR_start 序列在数组中的起始位置
 * @param NLR_end 序列在数组中的结束位置
 * @return BiTree 创建的二叉树
 */
BiTree BiTCreate(int *LNR, int LNR_start, int LNR_end, int *NLR, int NLR_start, int NLR_end)
{
	int mid;
	if(NLR_start > NLR_end || LNR_start > LNR_end) return NULL;
	for(mid = LNR_start; LNR[mid] != NLR[NLR_start]; mid++);
	BiTNode *N = (BiTNode*)malloc(sizeof(BiTNode));
	N->data = NLR[NLR_start];
	N->l = BiTCreate(LNR, LNR_start, mid - 1, NLR, NLR_start + 1, NLR_start + mid - LNR_start);
	N->r = BiTCreate(LNR, mid + 1, LNR_end,   NLR, NLR_start + mid - LNR_start + 1, NLR_end);
    return N;
}

/**
 * @brief 访问
 * @param T 待遍历的结点
 */
void visit(BiTNode *T)
{
	if(!T) return;
    printf("%d ", T->data);
}

/**
 * @brief 递归方式前序遍历
 * @param T 待遍历的二叉树
 */
void NLR(BiTree T)
{
	if(!T) return; //别忘了判空！！！
    visit(T);
	NLR(T->l);
	NLR(T->r);
}

/**
 * @brief 递归方式中序遍历
 * @param T 待遍历的二叉树
 */
void LNR(BiTree T)
{
	if(!T) return; //别忘了判空！！！
	LNR(T->l);
    visit(T);
	LNR(T->r);
}

/**
 * @brief 递归方式后序遍历
 * @param T 待遍历的二叉树
 */
void LRN(BiTree T)
{
	if(!T) return; //别忘了判空！！！
	LRN(T->l);
	LRN(T->r);
    visit(T);
}

/**
 * @brief 迭代方式前序遍历
 * @param T 待遍历的二叉树
 */
void NLR2(BiTree T)
{
	BiTNode *s[MAX], *p = T;
	int top = -1;
	while(top > -1 || p) //不用单独判断树是否为空，因为那样无法进入循环
	{
		if(p) //一路向左
		{
            visit(p);
			s[++top] = p;
			p = p->l;
		}
		else //左没了向右
		{
			p = s[top--];
			p = p->r;
		}
	}
}

/**
 * @brief 迭代方式中序遍历
 * @param T 待遍历的二叉树
 */
void LNR2(BiTree T)
{
	BiTNode *s[MAX], *p = T;
	int top = -1;
	while(top > -1 || p) //不用单独判断树是否为空，因为那样无法进入循环
	{
		if(p) //一路向左
		{
			s[++top] = p;
			p = p->l;
		}
		else //左没了向右
		{
			p = s[top--];
            visit(p);
			p = p->r;
		}
	}
}

/**
 * @brief 迭代方式后序遍历
 * @param T 待遍历的二叉树
 */
void LRN2(BiTree T)
{
	BiTNode *s[MAX], *p = T, *last = NULL; //last用来存储上一次访问的结点
	int top = -1;
	while(top > -1 || p) //不用单独判断树是否为空，因为那样无法进入循环
	{
		if(p) //一路向左
		{
			s[++top] = p;
            p = p->l;
		}
		else //左没了向右
		{
            BiTNode *t = s[top--];
			if(t->r && t->r != last) //如果存在右结点且未访问
			{
                s[++top] = t; //出栈出早了，再塞回去
				p = t->r;
			}
			else
			{
                visit(t);
				last = t;
			}
		}
	}
}

/**
 * @brief 层序遍历
 * @param T 待遍历的二叉树
 */
void Level(BiTree T)
{
	BiTNode *q[MAX];
	int rear = -1, head = -1;
	if(T)q[++rear] = T; //别忘了判空！！！
	while(head < rear)
	{
        BiTNode *t = q[++head];
		if(t->l)q[++rear] = t->l;
		if(t->r)q[++rear] = t->r;
        visit(t); //先把孩子入队再访问，方便处理一些记录每层结点数的题目
	}
}

/**
 * @brief 主函数
 * @return int
 */
int main()
{
    int N;

    puts("--------------------------------");
    puts("输入");
    puts("--------------------------------");

    puts("结点数：");
    scanf("%d", &N);
    int NLR_Array[N], LNR_Array[N];

    puts("前序序列：");
    for(int i = 0; i < N; i++)
    {
        scanf("%d", &NLR_Array[i]);
    }

    puts("中序序列：");
    for(int i = 0; i < N; i++)
    {
        scanf("%d", &LNR_Array[i]);
    }

    BiTree T = BiTCreate(LNR_Array, 0, N - 1, NLR_Array, 0, N - 1);

    puts("--------------------------------");
    puts("输出");
    puts("--------------------------------");

    puts("递归前序：");
    NLR(T);

    puts("\n迭代前序：");
    NLR2(T);

    puts("\n递归中序：");
    LNR(T);

    puts("\n迭代中序：");
    LNR2(T);

    puts("\n递归后序：");
    LRN(T);

    puts("\n迭代后序：");
    LRN2(T);

    puts("\n层序：");
    Level(T);

    return 0;
}
